from django.shortcuts import redirect, render
from lab_2.models import Note
from lab_4.forms import NoteForm
# from django.http.response import HttpResponse
# from django.core import serializers

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_index.html', response)

def add_note(request): #diambil dari GeeksForGeeks
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    if request.method == "POST":
        # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect("/lab-4/") #abis disave, balik ke  halaman lab-3
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_note_list.html', response)