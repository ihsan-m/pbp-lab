import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home : Scaffold(
        backgroundColor: Color(0xFF1b1d24),
        appBar : AppBar(
          backgroundColor: Color(0xFF22252c),
          title: Text('HOME'),
        ),
        body : Center(
          child : Column(
            mainAxisAlignment : MainAxisAlignment.center,
            children : <Widget>[
              Image.asset('assets/logo.png', width: 300, height: 300,),
              SizedBox(height: 20),
              Text('SIPLASH', style : TextStyle(fontSize : 32.0, color : Colors.white)),
              SizedBox(height: 20),
              Text('Simple Planner for Staying at Home', style : TextStyle(fontSize : 26.0, color : Colors.white)),
              SizedBox(height: 20),
              TextButton(onPressed: (){},style : TextButton.styleFrom( backgroundColor: Colors.blue, minimumSize : Size(50.0,50.0)), child: Text('Log In', style : TextStyle(fontSize : 26.0, color : Colors.white)),),
              SizedBox(height: 20),
              TextButton(onPressed: (){},style : TextButton.styleFrom( backgroundColor: Colors.blue, minimumSize : Size(50.0,50.0)), child: Text('Sign Up', style : TextStyle(fontSize : 26.0, color : Colors.white)),),

            ],
          ),
        ),
      )



      // theme: ThemeData(
      // // This is the theme of your application.
      //   primarySwatch: Colors.blue,
      // ),
      // home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key, required this.title}) : super(key: key);

//   // This widget is the home page of your application.

//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;

//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headline4,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
