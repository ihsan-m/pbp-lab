import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
      const MaterialApp(
          home: MyApp()
      )
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final _formKey = GlobalKey<FormState>();

  Future<void> createDeadline(BuildContext context) async {
    return await showDialog(context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: const Color(0xff1b1d24),
          content: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Divider(color: Colors.white),
                  const SizedBox(height: 20),
                  const Text('Title :', style : TextStyle(fontSize : 24.0, color : Colors.white)),
                  const SizedBox(height: 10),
                  TextFormField(
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill out this field.";
                    },
                    decoration: InputDecoration(
                      fillColor : Colors.white,
                      filled : true,
                      isDense: true,
                      hintText: "Title...",
                      hintStyle: const TextStyle(color: Colors.grey),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),

                  const Text('Date and Time :', style : TextStyle(fontSize : 24.0, color : Colors.white)),
                  const SizedBox(height: 10),
                  TextFormField(
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill out this field.";
                    },
                    decoration: InputDecoration(
                      fillColor : Colors.white,
                      filled : true,
                      isDense: true,
                      hintText: "dd//mm//yyyy --:--",
                      hintStyle: const TextStyle(color: Colors.grey),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),

                  const Text('Description :', style : TextStyle(fontSize : 24.0, color : Colors.white)),
                  const SizedBox(height: 10),
                  TextFormField(
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill out this field.";
                    },
                    minLines: 3,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      fillColor : Colors.white,
                      filled : true,
                      isDense: true,
                      hintText: "Description...",
                      hintStyle: const TextStyle(color: Colors.grey),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
          title: const Text('New Deadline', style : TextStyle(fontSize : 24.0, color : Colors.white)),

          actions: <Widget>[
            TextButton(
                onPressed: () {
                  if(_formKey.currentState!.validate()) {
                    Navigator.of(context).pop();
                  }
                },
                child: const Text("Create"))
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFF1b1d24)),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
              title: const Text("DEADLINE"),
              backgroundColor: const Color(0xFF22252c)
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await createDeadline(context);
            },
            child: const Icon(Icons.add),
          ),
          body: Center(
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(20),
                  child: const Text('My Deadlines', style : TextStyle(fontSize : 32.0, color : Colors.white)),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Upcoming', style : TextStyle(fontSize : 24.0, color : Colors.red)),
                      Divider(color: Colors.white),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Done', style : TextStyle(fontSize : 24.0, color : Colors.green)),
                      Divider(color: Colors.white),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
