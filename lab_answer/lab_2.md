Muhammad Hilmi Ihsan Mumtaaz - 2006523552
Sistem Informasi, PBP - F

1. Apakah perbedaan antara JSON dan XML?
JSON (JavaScript Object Notation) dan XML (Extensible Markup Language) keduanya
berperan sebagai penyimpan dan pengirim data. Namun, perbedaan diantaranya adalah :

- Berdasarkan namanya sendiri , JSON adalah "Object Notation" yang berarti 
merepresentasi objek, sedangkan XML adalah "Extensible Markup Language" yang berarti
penyedia framework untuk mendefinisikan markup language.
- JSON didasarkan dari bahasa JavaScript, sedangkan XML diambil dari SGML.
- JSON digunakan untuk merepresentasikan objek, sedangkan XML menggunakan struktur "tag"
untuk membawa data.
- File yang dimiliki JSON lebih mudah dibaca dibandingkan dengan file XML.
- Dari segi keamanannya, XML dikatakan lebih aman dibandingkan dengan JSON.

Sumber : https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
HTML (Hyper Text Markup Language) dan XML (Extensible Markup Language) keduanya
merupakan sebuah "Markup Language". Keduanya dapat digunakan untuk membuat
halaman web dan aplikasi web. Namun, perbedaan diantaranya adalah :

- HTML digunakan untuk menampilkan data, sedangkan XML digunakan untuk membawa/menyimpan data.
- HTML merupakan markup language, sedangkan XML merupakan penyedia framework untuk
mendefinisikan markup language itu.
- Tags yang ada di HTML terbatas, sedangkan tags pada XML dapat diperbanyak.
- Tags yang ada di HTML digunakan untuk menampilkan data, sedangkan tags pada XML
digunakan untuk menyatakan datanya dan bukan untuk menampilkannya.

Sumber : https://www.geeksforgeeks.org/html-vs-xml/